/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.academico.bean;

import br.com.senac.academico.dao.ProfessorDAO;
import br.com.senac.academico.model.Professor;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author Administrador
 */
@Named(value = "pesquisaProfessorBean")
@ViewScoped
public class PesquisaProfessorBean extends Bean {

    private Professor professorSelecionado;
    private List<Professor> lista;
    private ProfessorDAO dao;

    private String codigo;
    private String nome;

    public PesquisaProfessorBean() {
    }

    @PostConstruct
    public void init() {
        try {
            dao = new ProfessorDAO();
            professorSelecionado = new Professor();
            lista = dao.findAll();

        } catch (Exception ex) {
            ex.printStackTrace();
            this.addMessageErro("Falha ao carregar itens.");
        }

    }

    public void pesquisa() {
        try {
            this.lista = this.dao.findByFiltro(codigo, nome);

        } catch (Exception ex) {
            ex.printStackTrace();

        }

    }

    public void salvar() {

        if (this.professorSelecionado.getId() == 0) {
            this.dao.save(professorSelecionado);
            this.addMessageInfo("Salvo com sucesso!");
        } else {
            this.dao.update(professorSelecionado);
            this.addMessageInfo("Alterado com sucesso!");
        }

        this.pesquisa();
    }

    public void remover(Professor professor) {
        try {
            this.dao.delete(professor);
            this.addMessageInfo("Removido com sucesso!");
            this.pesquisa();
        } catch (Exception ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                this.addMessageErro("Não é possível remover alunos cadastrados em cursos.");
            } else {
                this.addMessageErro("Falha ao remover aluno.");
                ex.printStackTrace();
            }

        }
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Professor getProfessorSelecionado() {
        return professorSelecionado;
    }

    public void setProfessorSelecionado(Professor professorSelecionado) {
        this.professorSelecionado = professorSelecionado;
    }

    public List<Professor> getLista() {
        return lista;
    }

    public void setLista(List<Professor> lista) {
        this.lista = lista;
    }

}
