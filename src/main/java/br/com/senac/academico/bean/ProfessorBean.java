
package br.com.senac.academico.bean;

import br.com.senac.academico.dao.ProfessorDAO;
import br.com.senac.academico.model.Professor;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;


@Named(value = "professorBean")
@ViewScoped
public class ProfessorBean extends Bean {

    private Professor professor;
    private ProfessorDAO dao;

    public ProfessorBean() {
    }

    @PostConstruct
    public void init() {
        this.novo();
        this.dao = new ProfessorDAO();
    }

    public void salvar() {

        if (this.professor.getId() == 0) {
            this.dao.save(professor);
            this.addMessageInfo("Salvo com sucesso!");
        } else {
            this.dao.update(professor);
            this.addMessageInfo("Alterado com sucesso!");
        }
    }

    public void novo() {
        this.professor = new Professor();
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

}
