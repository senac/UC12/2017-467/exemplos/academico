/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.academico.bean;

import br.com.senac.academico.dao.AlunoDAO;
import br.com.senac.academico.model.Aluno;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "alunoBean")
@ViewScoped
public class AlunoBean extends Bean{

    
    private Aluno aluno ; 
    private AlunoDAO dao ; 
    
    
    public AlunoBean() {
    }
    
    @PostConstruct
    public void init(){
        this.novo();
        this.dao = new AlunoDAO() ; 
    }
    
    
    
    public void salvar(){
        
        if(this.aluno.getId() == 0 ){
            this.dao.save(aluno);
            this.addMessageInfo("Salvo com sucesso!");
        }else{
            this.dao.update(aluno);
            this.addMessageInfo("Alterado com sucesso!");
        }
    }
    
    
    public void novo(){
        this.aluno = new Aluno();
    }
    

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }
    
    
    
    
   
    
    
    
}
